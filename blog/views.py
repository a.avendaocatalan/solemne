from django.shortcuts import render
from blog.models import Post

def home(request):
    template_name = 'index.html'
    data = {}
    # select * from blog WHERE status = 1
    data['posts'] = Post.objects.filter(status=True)
    data['destacadas'] = Post.objects.filter(destacada=True).order_by('created_at')

    return render(request, template_name, data)

def detalleNoticia(request, id):
    template_name = 'noticia_detalle.html'
    data = {}
    # select * from blog WHERE status = 1
    data['posts'] = Post.objects.filter(status=True)

    return render(request, template_name, data)