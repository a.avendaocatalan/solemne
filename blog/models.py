from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=144)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Post(models.Model):
    title = models.CharField(max_length=144)
    content = models.TextField()
    date_published = models.DateField()
    status = models.BooleanField(default=False)
    destacada = models.BooleanField(default=False)
    sort_order = models.IntegerField(default=0)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    model_pic = models.ImageField(upload_to = 'Noticias')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
